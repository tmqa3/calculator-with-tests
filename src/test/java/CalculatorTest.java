import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;

/**
 * Created by andriiz on 07.07.17.
 */
public class CalculatorTest {

    @Test
    public void testAdd(){
        double result = Main.returnResult(1, 2, "+");

        assertEquals(result, 3.0);
    }
    @Test
    public void testMinus(){
        double result = Main.returnResult(2,1,"-");

        assertEquals(result,1.0);
    }
    @Test
    public void testDiv(){
        double result = Main.returnResult(10,2,"/");

        assertEquals(result,5.0);
    }
    @Test
    public void testMultip(){
        double result = Main.returnResult(10,2,"*");

        assertEquals(result,20.0);
    }
    @Test
    public void testInvalidValue(){
        double result = Main.returnResult(5,5,"asdas");

        assertEquals(result,0.0);
    }
}
