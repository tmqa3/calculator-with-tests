import java.util.InputMismatchException;
import java.util.Scanner;

/**
 * Created by andriiz on 28.06.17.
 */
public class Main {
    public static void main(String[] args) {
        boolean i = true;
        while (i) {
            Scanner scan = new Scanner(System.in);
            double a, b, result;
            String operator;
            try {
                System.out.println("Enter number a: " );
                a = scan.nextDouble();

                System.out.print("Enter some operator (Please use: /,*,-,+): ");
                operator = scan.next();

                System.out.println("Enter number b: ");
                b = scan.nextDouble();

                result = returnResult(a, b, operator);
                System.out.println("Your result: " + a + operator + b + "=" + result);
                // ask do you Continue start

                boolean j = true;
                while (j) {
                    System.out.println("Do you want continue? yes/no");
                    String questionContinue = scan.nextLine();

                    switch (questionContinue) {
                        case "yes":
                            i = i;
                            j = false;
                            break;
                        case "no":
                            i = false;
                            j = false;
                            break;
                        default:
                            System.out.println("Plese say yes or no");
                            break;
                    }//end switch
                }//end while j



            } catch (InputMismatchException e) {
                System.out.println("Entered value is not a number. Please enter a number");
                // ask do you Continue start
                String clearStr = scan.nextLine();
                clearStr = "";
                boolean j = true;
                while (j) {
                    System.out.println("Do you want continue? yes/no");
                    String questionContinue = scan.nextLine();

                    switch (questionContinue) {
                        case "yes":
                            i = i;
                            j = false;
                            break;
                        case "no":
                            i = false;
                            j = false;
                            break;
                        default:
                            System.out.println("Plese say yes or no");
                            break;
                    }//end switch
                }//end while j

                // ask do you Continue end
            }
        }
    }

    public static double returnResult(double a, double b, String operator) {
        switch (operator){
            case "+":
                return a + b;
            case "-":
                return a - b;
            case "*":
                return a * b;
            case "/":
                return a / b;
            default:
                System.out.println("ERROR! Operator is wrong");
        }
        return "error";
    }

    /*static String askContinueStr(){
        String askCon = "Do you want continue? yes/no";
        return askCon;

    }*/

  /*  public class TestMain{
        public void testReturnResult(){
            Calc c = new Calc();
            int res = c.returnResult(4,5,"+");
            if(9!=res){fail("error tests!")}
        }
    }*/

   /* static boolean askContinue(String questionContinue,boolean i) {
            switch (questionContinue) {
                case "yes":
                    i = i;
                    return i;
                case "no":
                    i = false;
                    return i;
                default:
                    System.out.println("Plese say yes or no");
            }
        return i;
    }*/

}


